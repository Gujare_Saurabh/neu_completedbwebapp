
-- Create Database web_student_tracker
CREATE DATABASE IF NOT EXISTS web_student_tracker;
USE web_student_tracker;

-- Create student Table

DROP TABLE IF EXISTS student;

CREATE TABLE IF NOT EXISTS student(
	id	int(9)NOT NULL AUTO_INCREMENT PRIMARY KEY,
    firstName varchar(45) DEFAULT NULL,
    lastName varchar(45) DEFAULT NULL,
    email varchar(100) DEFAULT NULL
)ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Insert some initial data

LOCK TABLE student WRITE;

INSERT INTO student 
	VALUES (1,'Mary','Public','mary@gmail.com'),
		(2,'John','Doe','john@gmail.com'),
        (3,'Ajay','Rao','ajay@gmail.com'),
        (4,'Bill','Neely','bill@gmail.com'),
        (5,'Maxwell','Dixon','max@gmail.com');

UNLOCK TABLES;

