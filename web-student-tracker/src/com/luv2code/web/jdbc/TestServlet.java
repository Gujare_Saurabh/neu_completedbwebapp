package com.luv2code.web.jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;



/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//Define datasource/connection pool for resource injection
	@Resource(name="jdbc/web_student_tracker")
	private DataSource dataSource;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Step1: Setup a print writer
		PrintWriter out = response.getWriter();
		response.setContentType("text/plain");
		
		//Step2: Get a connection to a database
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;
		try {
			myConn = dataSource.getConnection();
			
			//Step3: Create a SQL statement
			String sql="SELECT * FROM student";
			myStmt = myConn.createStatement();
			
			//Step4: Execute SQL Query
			myRs = myStmt.executeQuery(sql);
			
			//Step5: Process the result set
			while(myRs.next()) {
				String email = myRs.getString("email");
				out.println(email);
			}
			
		}catch(Exception exc) {
			exc.printStackTrace();
		}	
	}

}
